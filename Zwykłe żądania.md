# Zwykłe żądania

## Plan lekcji

Żądanie `POST`:

```
${adres REST API}/${symbol}/${symbol jednostki sprawozdawczej}/mobile-api/Uczen.v3.Uczen/PlanLekcjiZeZmianami
```

```json
{
    "DataPoczatkowa": "2018-04-23",
    "DataKoncowa": "2018-04-29",
    "IdOddzial": 97,
    "IdOkresKlasyfikacyjny": 602,
    "IdUczen": 2560,
    "RemoteMobileTimeKey": 1524661083,
    "TimeKey": 1524661082,
    "RequestId": "${random UUID v4}",
    "RemoteMobileAppVersion": "18.4.1.388",
    "RemoteMobileAppName": "VULCAN-Android-ModulUcznia"
}
```

Przykładowa odpowiedź:

```json
{
    "Status": "Ok",
    "TimeKey": 1524661085,
    "TimeValue": "2018.04.25 14:58:05",
    "RequestId": "ad0c0929-675f-45ad-9b8c-4202072d4205",
    "DayOfWeek": 3,
    "AppVersion": "17.09.0009.26859",
    "Data": [
        {
            "Dzien": 1524607200,
            "DzienTekst": "2018-04-25",
            "NumerLekcji": 2,
            "IdPoraLekcji": 77,
            "IdPrzedmiot": 485,
            "PrzedmiotNazwa": "Chemia",
            "PodzialSkrot": null,
            "Sala": "201",
            "IdPracownik": 142,
            "IdPracownikWspomagajacy": null,
            "IdPracownikOld": null,
            "IdPracownikWspomagajacyOld": null,
            "IdPlanLekcji": 41,
            "AdnotacjaOZmianie": "(nieobecność: Uczniowie przychodzą później)",
            "PrzekreslonaNazwa": true,
            "PogrubionaNazwa": false,
            "PlanUcznia": true
        },
        {
            "Dzien": 1524520800,
            "DzienTekst": "2018-04-24",
            "NumerLekcji": 5,
            "IdPoraLekcji": 80,
            "IdPrzedmiot": 429,
            "PrzedmiotNazwa": "Język angielski",
            "PodzialSkrot": null,
            "Sala": "206",
            "IdPracownik": 168,
            "IdPracownikWspomagajacy": null,
            "IdPracownikOld": 149,
            "IdPracownikWspomagajacyOld": null,
            "IdPlanLekcji": 44,
            "AdnotacjaOZmianie": "(zastępstwo: Jan Bytnar, sala 206)",
            "PrzekreslonaNazwa": false,
            "PogrubionaNazwa": false,
            "PlanUcznia": true
        },
        {
            "Dzien": 1524693600,
            "DzienTekst": "2018-04-26",
            "NumerLekcji": 2,
            "IdPoraLekcji": 77,
            "IdPrzedmiot": 483,
            "PrzedmiotNazwa": "Fizyka",
            "PodzialSkrot": null,
            "Sala": "215",
            "IdPracownik": 178,
            "IdPracownikWspomagajacy": null,
            "IdPracownikOld": null,
            "IdPracownikWspomagajacyOld": null,
            "IdPlanLekcji": 42,
            "AdnotacjaOZmianie": "(nieobecność: Uczniowie przychodzą później)",
            "PrzekreslonaNazwa": true,
            "PogrubionaNazwa": false,
            "PlanUcznia": true
        },
        {
            "Dzien": 1524434400,
            "DzienTekst": "2018-04-23",
            "NumerLekcji": 5,
            "IdPoraLekcji": 80,
            "IdPrzedmiot": 483,
            "PrzedmiotNazwa": "Fizyka",
            "PodzialSkrot": null,
            "Sala": "213",
            "IdPracownik": 178,
            "IdPracownikWspomagajacy": null,
            "IdPracownikOld": null,
            "IdPracownikWspomagajacyOld": null,
            "IdPlanLekcji": 23,
            "AdnotacjaOZmianie": "",
            "PrzekreslonaNazwa": false,
            "PogrubionaNazwa": false,
            "PlanUcznia": true
        },
        {
            "Dzien": 1524607200,
            "DzienTekst": "2018-04-25",
            "NumerLekcji": 1,
            "IdPoraLekcji": 76,
            "IdPrzedmiot": 482,
            "PrzedmiotNazwa": "Matematyka",
            "PodzialSkrot": null,
            "Sala": "203",
            "IdPracownik": 183,
            "IdPracownikWspomagajacy": null,
            "IdPracownikOld": null,
            "IdPracownikWspomagajacyOld": null,
            "IdPlanLekcji": 43,
            "AdnotacjaOZmianie": "(nieobecność: Uczniowie przychodzą później)",
            "PrzekreslonaNazwa": true,
            "PogrubionaNazwa": false,
            "PlanUcznia": true
        },
        {
            "Dzien": 1524607200,
            "DzienTekst": "2018-04-25",
            "NumerLekcji": 4,
            "IdPoraLekcji": 79,
            "IdPrzedmiot": 499,
            "PrzedmiotNazwa": "Wychowanie fizyczne",
            "PodzialSkrot": "wf_grupa_2",
            "Sala": "S4",
            "IdPracownik": 146,
            "IdPracownikWspomagajacy": null,
            "IdPracownikOld": 189,
            "IdPracownikWspomagajacyOld": null,
            "IdPlanLekcji": 46,
            "AdnotacjaOZmianie": "(zastępstwo: Anon Gałecki, sala S4)",
            "PrzekreslonaNazwa": false,
            "PogrubionaNazwa": false,
            "PlanUcznia": true
        },
        {
            "Dzien": 1524693600,
            "DzienTekst": "2018-04-26",
            "NumerLekcji": 7,
            "IdPoraLekcji": 82,
            "IdPrzedmiot": 495,
            "PrzedmiotNazwa": "Informatyka",
            "PodzialSkrot": "informatyka_2",
            "Sala": "115",
            "IdPracownik": 1773,
            "IdPracownikWspomagajacy": null,
            "IdPracownikOld": null,
            "IdPracownikWspomagajacyOld": null,
            "IdPlanLekcji": 38,
            "AdnotacjaOZmianie": "",
            "PrzekreslonaNazwa": false,
            "PogrubionaNazwa": false,
            "PlanUcznia": false
        },
        {
            "Dzien": 1524693600,
            "DzienTekst": "2018-04-26",
            "NumerLekcji": 6,
            "IdPoraLekcji": 81,
            "IdPrzedmiot": 488,
            "PrzedmiotNazwa": "Geografia",
            "PodzialSkrot": null,
            "Sala": "206",
            "IdPracownik": 149,
            "IdPracownikWspomagajacy": null,
            "IdPracownikOld": null,
            "IdPracownikWspomagajacyOld": null,
            "IdPlanLekcji": 4,
            "AdnotacjaOZmianie": "",
            "PrzekreslonaNazwa": false,
            "PogrubionaNazwa": false,
            "PlanUcznia": true
        }
    ]
}
```

## Oceny cząstkowe

Żądanie `POST`:

```
${adres REST API}/${symbol}/${symbol jednostki sprawozdawczej}/mobile-api/Uczen.v3.Uczen/Oceny
```

```json
{
    "IdOkresKlasyfikacyjny": 602,
    "IdUczen": 2560,
    "RemoteMobileTimeKey": 1524759512,
    "TimeKey": 1524759511,
    "RequestId": "${random UUID v4}",
    "RemoteMobileAppVersion": "18.4.1.388",
    "RemoteMobileAppName": "VULCAN-Android-ModulUcznia"
}
```

Przykładowa odpowiedź:

```json
{
    "Status": "Ok",
    "TimeKey": 1524759514,
    "TimeValue": "2018.04.26 18:18:33",
    "RequestId": "bbcf7769-5513-484a-a71d-e0a17d92bf4e",
    "DayOfWeek": 4,
    "AppVersion": "17.09.0009.26859",
    "Data": [
        {
            "Id": 1676947,
            "Pozycja": 3,
            "PrzedmiotPozycja": 8,
            "IdPrzedmiot": 474,
            "IdKategoria": 29,
            "Wpis": "1",
            "Wartosc": 1.00,
            "WagaModyfikatora": null,
            "WagaOceny": 5.00,
            "Licznik": null,
            "Mianownik": null,
            "Komentarz": null,
            "Waga": "5,00",
            "Opis": "spr.-rozbiory",
            "DataUtworzenia": 1517231281,
            "DataUtworzeniaTekst": "2018-01-29",
            "DataModyfikacji": 1517231281,
            "DataModyfikacjiTekst": "2018-01-29",
            "IdPracownikD": 164,
            "IdPracownikM": 164
        },
        {
            "Id": 1681941,
            "Pozycja": 9,
            "PrzedmiotPozycja": 3,
            "IdPrzedmiot": 429,
            "IdKategoria": 83,
            "Wpis": "95",
            "Wartosc": 95.00,
            "WagaModyfikatora": null,
            "WagaOceny": 0.00,
            "Licznik": null,
            "Mianownik": null,
            "Komentarz": "%",
            "Waga": "0,00",
            "Opis": "egz. prób. podstawa %",
            "DataUtworzenia": 1517479168,
            "DataUtworzeniaTekst": "2018-02-01",
            "DataModyfikacji": 1517479168,
            "DataModyfikacjiTekst": "2018-02-01",
            "IdPracownikD": 152,
            "IdPracownikM": 152
        },
        {
            "Id": 1684515,
            "Pozycja": 13,
            "PrzedmiotPozycja": 14,
            "IdPrzedmiot": 482,
            "IdKategoria": 70,
            "Wpis": "6-",
            "Wartosc": 6.00,
            "WagaModyfikatora": -0.25,
            "WagaOceny": 3.00,
            "Licznik": null,
            "Mianownik": null,
            "Komentarz": null,
            "Waga": "3,00",
            "Opis": "Egzamin próbny - styczeń 2018",
            "DataUtworzenia": 1517693198,
            "DataUtworzeniaTekst": "2018-02-03",
            "DataModyfikacji": 1517693838,
            "DataModyfikacjiTekst": "2018-02-03",
            "IdPracownikD": 183,
            "IdPracownikM": 183
        },
        {
            "Id": 1716982,
            "Pozycja": 28,
            "PrzedmiotPozycja": 10,
            "IdPrzedmiot": 488,
            "IdKategoria": 28,
            "Wpis": "4+",
            "Wartosc": 4.00,
            "WagaModyfikatora": 0.25,
            "WagaOceny": 3.00,
            "Licznik": null,
            "Mianownik": null,
            "Komentarz": null,
            "Waga": "3,00",
            "Opis": "przemysł",
            "DataUtworzenia": 1519154969,
            "DataUtworzeniaTekst": "2018-02-20",
            "DataModyfikacji": 1519154969,
            "DataModyfikacjiTekst": "2018-02-20",
            "IdPracownikD": 149,
            "IdPracownikM": 149
        },
        {
            "Id": 1807239,
            "Pozycja": 62,
            "PrzedmiotPozycja": 2,
            "IdPrzedmiot": 428,
            "IdKategoria": 28,
            "Wpis": "3",
            "Wartosc": 3.00,
            "WagaModyfikatora": null,
            "WagaOceny": 0.00,
            "Licznik": null,
            "Mianownik": null,
            "Komentarz": null,
            "Waga": "0,00",
            "Opis": "",
            "DataUtworzenia": 1523820074,
            "DataUtworzeniaTekst": "2018-04-15",
            "DataModyfikacji": 1523820074,
            "DataModyfikacjiTekst": "2018-04-15",
            "IdPracownikD": 176,
            "IdPracownikM": 176
        },
        {
            "Id": 1811544,
            "Pozycja": 65,
            "PrzedmiotPozycja": 2,
            "IdPrzedmiot": 428,
            "IdKategoria": 27,
            "Wpis": "",
            "Wartosc": null,
            "WagaModyfikatora": null,
            "WagaOceny": 0.00,
            "Licznik": null,
            "Mianownik": null,
            "Komentarz": "bz",
            "Waga": "0,00",
            "Opis": "",
            "DataUtworzenia": 1523957481,
            "DataUtworzeniaTekst": "2018-04-17",
            "DataModyfikacji": 1523957481,
            "DataModyfikacjiTekst": "2018-04-17",
            "IdPracownikD": 176,
            "IdPracownikM": 176
        }
    ]
}
```

## Oceny przewidywane, końcowe, średnie ocen

**Średnie ocen nie są podawane, jeśli ta możliwość zostanie wyłączona przez szkołę**, co jest bardzo częstą praktyką.

Żądanie `POST`:

```
${adres REST API}/${symbol}/${symbol jednostki sprawozdawczej}/mobile-api/Uczen.v3.Uczen/OcenyPodsumowanie
```

```json
{
    "IdOkresKlasyfikacyjny": 602,
    "IdUczen": 2560,
    "RemoteMobileTimeKey": 1524759512,
    "TimeKey": 1524759511,
    "RequestId": "${random UUID v4}",
    "RemoteMobileAppVersion": "18.4.1.388",
    "RemoteMobileAppName": "VULCAN-Android-ModulUcznia"
}
```

Przykładowa odpowiedź:

```json
{
    "Status": "Ok",
    "TimeKey": 1524759513,
    "TimeValue": "2018.04.26 18:18:33",
    "RequestId": "6fbd682a-8faa-4949-bec6-97fa05a776ec",
    "DayOfWeek": 4,
    "AppVersion": "17.09.0009.26859",
    "Data": {
      "OcenyPrzewidywane":[
         {
            "IdPrzedmiot":1078,
            "Wpis":"4"
         },
         {
            "IdPrzedmiot":1081,
            "Wpis":"3"
         },
         {
            "IdPrzedmiot":1082,
            "Wpis":"4"
         },
         {
            "IdPrzedmiot":1086,
            "Wpis":"4"
         },
         {
            "IdPrzedmiot":1128,
            "Wpis":"4"
         }
      ],
      "OcenyKlasyfikacyjne":[
         {
            "IdPrzedmiot":6248,
            "Wpis":"5"
         }
      ],
      "SrednieOcen":[
         {
            "IdPrzedmiot":6255,
            "SredniaOcen":"3,21",
            "SumaPunktow":""
         },
         {
            "IdPrzedmiot":1135,
            "SredniaOcen":"3,16",
            "SumaPunktow":""
         },
         {
            "IdPrzedmiot":1086,
            "SredniaOcen":"4,56",
            "SumaPunktow":""
         },
         {
            "IdPrzedmiot":1081,
            "SredniaOcen":"3,25",
            "SumaPunktow":""
         },
         {
            "IdPrzedmiot":1298,
            "SredniaOcen":"4,33",
            "SumaPunktow":""
         }
      ]
    }
}
```

## Terminy sprawdzianów

```
${adres REST API}/${symbol}/${symbol jednostki sprawozdawczej}/mobile-api/Uczen.v3.Uczen/Sprawdziany
```

```json
{
    "DataPoczatkowa": "2018-03-26",
    "DataKoncowa": "2018-08-31",
    "IdOddzial": 97,
    "IdOkresKlasyfikacyjny": 602,
    "IdUczen": 2560,
    "RemoteMobileTimeKey": 1524759504,
    "TimeKey": 1524759503,
    "RequestId": "${random UUID v4}",
    "RemoteMobileAppVersion": "18.4.1.388",
    "RemoteMobileAppName": "VULCAN-Android-ModulUcznia"
}
```

```json
{
    "Status": "Ok",
    "TimeKey": 1524759507,
    "TimeValue": "2018.04.26 18:18:27",
    "RequestId": "f92d3e94-1756-41bc-8167-889c3901122e",
    "DayOfWeek": 4,
    "AppVersion": "17.09.0009.26859",
    "Data": [
        {
            "Id": 21558,
            "IdPrzedmiot": 482,
            "IdPracownik": 183,
            "IdOddzial": 97,
            "IdPodzial": null,
            "PodzialNazwa": null,
            "PodzialSkrot": null,
            "Rodzaj": true,
            "Opis": "Figury na płaszczyźnie.",
            "Data": 1522101600,
            "DataTekst": "2018-03-27"
        },
        {
            "Id": 22067,
            "IdPrzedmiot": 429,
            "IdPracownik": 152,
            "IdOddzial": 97,
            "IdPodzial": 2067,
            "PodzialNazwa": "język_angielski_gr_1",
            "PodzialSkrot": "język_angi_gr_1",
            "Rodzaj": true,
            "Opis": "czasowniki nieregualne 1 częsć",
            "Data": 1523311200,
            "DataTekst": "2018-04-10"
        },
        {
            "Id": 23031,
            "IdPrzedmiot": 488,
            "IdPracownik": 149,
            "IdOddzial": 97,
            "IdPodzial": null,
            "PodzialNazwa": null,
            "PodzialSkrot": null,
            "Rodzaj": false,
            "Opis": "Opolszczyzna - mapa",
            "Data": 1526335200,
            "DataTekst": "2018-05-15"
        }
    ]
}
```

`Rodzaj` określa typ pracy - `true` to sprawdzian, `false` to kartkówka

## Uwagi

Żądanie `POST`:

```
${adres REST API}/${symbol}/${symbol jednostki sprawozdawczej}/mobile-api/Uczen.v3.Uczen/UwagiUcznia
```

```json
{
    "IdOkresKlasyfikacyjny": 602,
    "IdUczen": 2560,
    "RemoteMobileTimeKey": 1524763203,
    "TimeKey": 1524763202,
    "RequestId": "${random UUID v4}",
    "RemoteMobileAppVersion": "18.4.1.388",
    "RemoteMobileAppName": "VULCAN-Android-ModulUcznia"
}
```

Przykładowa odpowiedź:

```json
{
    "Status": "Ok",
    "TimeKey": 1524763204,
    "TimeValue": "2018.04.26 19:20:04",
    "RequestId": "1d453a80-120a-407a-8003-7bc554e45ca0",
    "DayOfWeek": 4,
    "AppVersion": "17.09.0009.26859",
    "Data": [
        {
            "IdPracownik": 152,
            "UczenImie": "Anon",
            "UczenNazwisko": "Anonowicz",
            "PracownikImie": "Janusz",
            "PracownikNazwisko": "Tracz",
            "DataWpisu": 1522074333,
            "DataWpisuTekst": "2018-03-26",
            "DataModyfikacji": 1522074333,
            "DataModyfikacjiTekst": null,
            "UwagaKey": "00000000-0000-0000-0000-000000000000",
            "Id": 30649,
            "TrescUwagi": "+ 20p za udział w Konkursie Języka Angielskiego",
            "IdUczen": 2560,
            "IdKategoriaUwag": 66
        }
    ]
}
```

## Frekwencja

Żądanie `POST`:

```
${adres REST API}/${symbol}/${symbol jednostki sprawozdawczej}/mobile-api/Uczen.v3.Uczen/Frekwencje
```

```json
{
    "DataPoczatkowa": "2018-04-23",
    "DataKoncowa": "2018-04-29",
    "IdOddzial": 97,
    "IdOkresKlasyfikacyjny": 602,
    "IdUczen": 2560,
    "RemoteMobileTimeKey": 1524763220,
    "TimeKey": 1524763219,
    "RequestId": "${random UUID v4}",
    "RemoteMobileAppVersion": "18.4.1.388",
    "RemoteMobileAppName": "VULCAN-Android-ModulUcznia"
}
```

Przykładowa odpowiedź:

```json
{
    "Status": "Ok",
    "TimeKey": 1524763222,
    "TimeValue": "2018.04.26 19:20:21",
    "RequestId": "18985343-3417-4e83-bae6-437a1fa633dd",
    "DayOfWeek": 4,
    "AppVersion": "17.09.0009.26859",
    "Data": {
        "DataPoczatkowa": 1524434400,
        "DataKoncowa": 1525039199,
        "DataPoczatkowaTekst": "2018-04-23",
        "DataKoncowaTekst": "2018-04-29",
        "Frekwencje": [
            {
                "IdKategoria": 1,
                "Numer": 1,
                "IdPoraLekcji": 76,
                "Dzien": 1524434400,
                "DzienTekst": "2018-04-23",
                "IdPrzedmiot": 474,
                "PrzedmiotNazwa": "Zajęcia artystyczne"
            },
            {
                "IdKategoria": 1,
                "Numer": 2,
                "IdPoraLekcji": 77,
                "Dzien": 1524434400,
                "DzienTekst": "2018-04-23",
                "IdPrzedmiot": 512,
                "PrzedmiotNazwa": "Informatyka"
            },
            {
                "IdKategoria": 1,
                "Numer": 3,
                "IdPoraLekcji": 78,
                "Dzien": 1524434400,
                "DzienTekst": "2018-04-23",
                "IdPrzedmiot": 428,
                "PrzedmiotNazwa": "Matematyka"
            },
            {
                "IdKategoria": 1,
                "Numer": 4,
                "IdPoraLekcji": 79,
                "Dzien": 1524434400,
                "DzienTekst": "2018-04-23",
                "IdPrzedmiot": 428,
                "PrzedmiotNazwa": "Matematyka"
            },
            {
                "IdKategoria": 1,
                "Numer": 3,
                "IdPoraLekcji": 78,
                "Dzien": 1524520800,
                "DzienTekst": "2018-04-24",
                "IdPrzedmiot": 480,
                "PrzedmiotNazwa": "Historia"
            }
        ]
    }
}
```

## Zadania domowe

Żądanie `POST`:

```
${adres REST API}/${symbol}/${symbol jednostki sprawozdawczej}/mobile-api/Uczen.v3.Uczen/ZadaniaDomowe
```

```json
{
    "DataPoczatkowa": "2018-03-26",
    "DataKoncowa": "2018-08-31",
    "IdOddzial": 97,
    "IdOkresKlasyfikacyjny": 602,
    "IdUczen": 2560,
    "RemoteMobileTimeKey": 1524763251,
    "TimeKey": 1524763250,
    "RequestId": "${random UUID v4}",
    "RemoteMobileAppVersion": "18.4.1.388",
    "RemoteMobileAppName": "VULCAN-Android-ModulUcznia"
}
```

Przykładowa odpowiedź:

```json
{
    "Status": "Ok",
    "TimeKey": 1524763253,
    "TimeValue": "2018.04.26 19:20:53",
    "RequestId": "866cf3c0-8f5b-4978-89e9-e2452b8a2f10",
    "DayOfWeek": 4,
    "AppVersion": "17.09.0009.26859",
    "Data": [
        {
            'Id': 185568,
            'IdUczen': 20607,
            'Data': 1558994400,
            'DataTekst': '2019-05-28',
            'IdPracownik': 6261,
            'IdPrzedmiot': 1547,
            'Opis': 'Project'
        }
    ]
}
```
